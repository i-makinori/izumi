

(in-package :asdf-user)


(defsystem izumi
  :author "i-makinori"
  :license "MIT"
  :description "izumi: reaction diffusion system simulator"
  :depends-on
  (#:mcclim
   #:mcclim-bezier
   #:bordeaux-threads)
  :components
  ((:file "package")
   (:module "src"
    :components
    ((:file "2d-matrix")
     (:file "reaction-diffusion-system") ;; rds
     (:file "rds-variables")
     ;; gui
     (:file "density-space")
     (:file "gui")
     )
    )
   ))