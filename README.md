# izumi

![izumi2019.png](./izumi2019.png)

## execute program

1. clone this repository under `asdf:*central-registry*`.
1. run lisp ReadEvalPrintLoop or eval `izumi.asd`.
1. `(ql:quickload :izumi)` ;; to load izumi and izumi's dependence.
1. `(izumi::run)` ;; and run izumi, pseudo reaction-diffusion-system GUI.

sample parameters are written in `src/rds-variables.lisp`.


## todo
- speed up functions
- save image
- mcclim-repl to structured user interface
- reconsider reaction-functions

## 反応拡散系について. Reaction Diffusion System


## 参考 reference

### 反応拡散系 reaction diffusion system
- http://www.fbs.osaka-u.ac.jp/labs/skondo/ozaki/what%20is%20RD%201%20by%20ozaki.htm

##### ミカエリス･メンテン式
- http://www.sc.fukuoka-u.ac.jp/~bc1/Biochem/EnzInh.htm

### commonlisp,mcclim

- mcclim 
  - https://github.com/McCLIM/McCLIM/blob/master/Examples/clim-fig.lisp
  - https://github.com/McCLIM/McCLIM/blob/master/Examples/logic-cube.lisp
- game
  - https://github.com/josrr/guerra-espacial
- lisp-matrix
  - https://github.com/blindglobe/lisp-matrix
- boardaux-threads
  - https://trac.common-lisp.net/bordeaux-threads/wiki/ApiDocumentation  

thank you
