
(in-package :izumi)

;;;; reaction-diffusion systems


;; struct 

(defstruct (reaction-diffusion-system (:conc-name rds-)
                                     (:constructor struct-rds))
  (reaction-a-to-a 0.00)
  (reaction-a-to-b 0.00)
  (reaction-b-to-a 0.00)
  (reaction-b-to-b 0.00)
  ;;
  (diffusion-a 0.00)
  (diffusion-b 0.00)
  ;;
  (space-a #2A(()))
  (space-b #2A(()))
  ;;
  (evaporation-a 1.00)
  (evaporation-b 1.00)
  ;;
  (current-time 0)
  ;;
  (space-height 0)
  (space-width 0))


(defun make-rds (height width diffusion-a diffusion-b evaporation-a evaporation-b)
  (struct-rds
   
   ;;
   :diffusion-a diffusion-a
   :diffusion-b diffusion-b
   ;;
   :evaporation-a evaporation-a
   :evaporation-b evaporation-b
   ;;
   :current-time 0
   :space-height height
   :space-width width
   :space-a
   (make-double-float-array height width)
   :space-b
   (make-double-float-array height width)))

(defun reset-reaction-rate (rds a-to-a a-to-b b-to-a b-to-b)
  (let ((new-rds (copy-structure rds)))
    (setf (rds-reaction-a-to-a new-rds) a-to-a)
    (setf (rds-reaction-a-to-b new-rds) a-to-b)
    (setf (rds-reaction-b-to-a new-rds) b-to-a)
    (setf (rds-reaction-b-to-b new-rds) b-to-b)
    new-rds
  ))
    

(defun rds-update-concentration-space (function rds)
  (setf (rds-space-a rds)
        (funcall function (rds-space-a rds)))
  (setf (rds-space-b rds)
        (funcall function (rds-space-b rds))
  ))


;;; space resettings

(defun reset-to-randomize-space (space)
  (do-2d-matrix space (y x)
    (setf (aref space y x) (coerce (random 1.00d0) 'double-float)))
  space)

(defun reset-to-united-value-space (space &optional (vale 0.00))
  (do-2d-matrix space (y x)
    (setf (aref space y x) vale))
  space)


(defun inject-concentration (space center-y center-x radius concentration)
  (labels 
      ((collision-detection (y-point x-point)
         (< (+ (expt (- y-point center-y) 2) (expt (- x-point center-x) 2))
            radius)))
    (do-2d-matrix space (y x)
      (if (collision-detection y x)
          (setf (aref space y x) concentration)))))

;;; update

(defun space-height (space)
  (array-dimension space 0))

(defun space-width (space)
  (array-dimension space 1))


;; reaction-diffusion system
;;
;; du/dt = F(u) + D*{d^2u}/(dx^2)
;; 

;;; 2-medium


(defun diffusion (cell link-cell-list edge-cell-list
                  diffusion-coefficien evaporation-coefficien)
  (let ((len (+ (length link-cell-list) (length edge-cell-list))))
    (* (+ (* (/ (- 1.00 diffusion-coefficien) 1)
             cell)
          (* (/ 1.4 (+ 1 1.4))
             (apply #'+ link-cell-list)
             (/ diffusion-coefficien
                len))
          (* (/ 1.0 (+ 1 1.4))
             (apply #'+ edge-cell-list)
             (/ diffusion-coefficien
                len))
          )
       evaporation-coefficien)))


(defun linked-cell-list (space y x)
  "next-space in torus-xy axis"
  (let ((height (space-height space))
        (width (space-width space)))
    (list (aref space y (mod (- x 1) width))
          (aref space y (mod (+ x 1) width))
          (aref space (mod (- y 1) height) x)
          (aref space (mod (+ y 1) height) x))))

(defun edge-cell-list (space y x)
  "next-space in torus-xy axis"
  (let ((height (space-height space))
        (width (space-width space)))
    (list (aref space (mod (- y 1) height) (mod (- x 1) width))
          (aref space (mod (- y 1) height) (mod (+ x 1) width))
          (aref space (mod (+ y 1) height) (mod (- x 1) width))
          (aref space (mod (+ y 1) height) (mod (+ x 1) width)))))


#|
(defun reaction-2d-cell-a (cell-a cell-d a-to-a d-to-a)
  (+ (* cell-a a-to-a)
     (* cell-d d-to-a)))
|#

(defun update-cell-a (func cell-a cell-d
                      link-cell-list-a edge-cell-list-a
                      a-to-a d-to-a
                      diffusion-coefficien evaporation-coefficien)
  "a:car-, d:cdr-"
  (let*
      ((diffusion 
         (diffusion cell-a link-cell-list-a edge-cell-list-a
                    diffusion-coefficien evaporation-coefficien))
       ;;
       (reaction (funcall func cell-a cell-d a-to-a d-to-a)))
    diffusion
    (max 0.0 (min 100.00
                  (+ diffusion reaction)))))




(defun bistable (cell-a cell-b a-to-a b-to-a)
  (+ (* a-to-a (* cell-a cell-a) (/ (+ 0.0000000001  cell-b)))
     (+ (* cell-b b-to-a))))


(defun adjustment-factor (cell-b cell-a a-to-b b-to-b)
  (+ (* a-to-b cell-a)
     (* b-to-b cell-b)
  ))


(defun update-rds (rds)
  (let* ((a-to-a (rds-reaction-a-to-a rds))
         (a-to-b (rds-reaction-a-to-b rds))
         (b-to-a (rds-reaction-b-to-a rds))
         (b-to-b (rds-reaction-b-to-b rds))
         ;;
         (diffusion-a (rds-diffusion-a rds))
         (diffusion-b (rds-diffusion-b rds))
         ;;
         (evaporation-a (rds-evaporation-a rds))
         (evaporation-b (rds-evaporation-a rds))
         ;;
         (space-a (rds-space-a rds))
         (space-b (rds-space-b rds))
         (next-space-a (make-array (array-dimensions space-a)))
         (next-space-b (make-array (array-dimensions space-b))))
    (do-2d-matrix space-a (uy ux)
      (setf (aref next-space-a uy ux)
            (update-cell-a
             #'bistable
             (aref space-a uy ux) (aref space-b uy ux)
             (linked-cell-list space-a uy ux)
             (edge-cell-list space-a uy ux)
             a-to-a b-to-a
             diffusion-a evaporation-a)))
    (do-2d-matrix space-b (uy ux)
      (setf (aref next-space-b uy ux)
            (update-cell-a
             #'bistable
             (aref space-b uy ux) (aref space-a uy ux)
             (linked-cell-list space-b uy ux)
             (edge-cell-list space-a uy ux)
             b-to-b a-to-b
             diffusion-b evaporation-b)))
    (values next-space-a next-space-b)))

