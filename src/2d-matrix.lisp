
(in-package :izumi)


;;; 2d-matrix utils

(defun make-double-float-array (height width &optional (value 0.00d0))
  (make-array (list height width)
              :element-type 'double-float
              :initial-element value))

(defmacro do-2d-matrix (space dimension-form &body body)
  `(dotimes (,(car dimension-form) (array-dimension ,space 0))
     (dotimes (,(cadr dimension-form) (array-dimension ,space 1))
       ,@body)))

(defun maximum (matrix)
  (let ((max-num (aref matrix 0 0)))
    (do-2d-matrix matrix (y x)
        (setq max-num
              (max max-num (aref matrix y x))))
    max-num))

(defun minimum (matrix)
  (let ((max-num (aref matrix 0 0)))
    (do-2d-matrix matrix (y x)
        (setq max-num
              (min max-num (aref matrix y x))))
    max-num))

(defun sum (matrix)
  (let ((aux 0))
    (do-2d-matrix matrix (y x)
      (incf aux (aref matrix y x)))
    aux))

(defun matrix-cell-length (matrix)
  (* (array-dimension matrix 0) 
     (array-dimension matrix 1)))

(defun matrix-average (matrix)
  (/ (sum matrix)
     (matrix-cell-length matrix)
  ))
  

