(in-package :izumi)



;;; parameter



(defparameter *current-calculation-rds*
  (make-rds 120 120 0.30 0.20 1.00 1.00))


(setf *current-calculation-rds*
      (reset-reaction-rate *current-calculation-rds*
                           ;;0.40 0.40 -0.80 -0.06))
                           ;;0.05 0.04 -0.20 -0.01))
                           0.03 0.01 -0.06 -0.03))
                           ;;0.08 0.10 -0.20 -0.15))
                           ;;0.00000 -100.05 -0.06 0.07))


(inject-concentration (rds-space-a *current-calculation-rds*)
                      60 60 12 0.80d0)


#|
;; randomize init

(progn
  (setf (rds-space-a *current-calculation-rds*)
        (reset-to-randomize-space
         (rds-space-a *current-calculation-rds*)))
  (setf (rds-space-b *current-calculation-rds*)
        (reset-to-randomize-space
         (rds-space-b *current-calculation-rds*))))
|#