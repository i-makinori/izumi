(in-package :izumi)


;;;; observe-device

(in-package :izumi)


;;; gui -- density-space


(defclass izumi-density-pane (clime::never-repaint-background-mixin basic-gadget)
  ((drawing-width-of-cell :initform 6 :accessor drawing-width-of-cell)
   (pointer-dragging :initform nil :accessor pointer-dragging)))

(defmethod compose-space ((pane izumi-density-pane) &key width height)
  width height
  (make-space-requirement :min-width 200 :min-height 200
                          :width 600 :height 600))


;;; draw

(defun float-color (val1 val2)
  ;;(declare (inline float-color))
  ;;(make-rgb-color (abs (sin value)) 0.00d0 (abs (cos value)))
  (make-rgb-color (min 1.00 (/ val1 22)) 0.00d0 (min 1.00 (/ val2 22))))
  

(defun draw-rds (rds)
  rds
  (let ((density-space-pane (find-pane-named *application-frame* 'density-space)))
    (repaint-sheet density-space-pane (sheet-region density-space-pane))))


(defmethod handle-repaint ((pane izumi-density-pane) region)
  (let* ((space-a (rds-space-a *current-calculation-rds*))
         (space-b (rds-space-b *current-calculation-rds*))
         (observe-modulo 1)
         (cell-draw-width (drawing-width-of-cell pane))
         (width/observe (/ cell-draw-width observe-modulo)))
    (do-2d-matrix space-a
        (y x)
      (if (and (= (mod y observe-modulo) 0) (= (mod x observe-modulo) 0))
          (draw-rectangle* pane
                           (* width/observe x)
                           (* width/observe y)
                           (+ cell-draw-width (* width/observe x))
                           (+ cell-draw-width (* width/observe y))
                           :ink (float-color (aref space-a y x)
                                             (aref space-b y x)
                           ))))))


;;; insert-density

(defun gui-inject-concentration (density-space-pane event-mouse-x event-mouse-y)
  (let* ((circle-radius (gadget-value (find-pane-named *application-frame* 'radius-value)))
         (concentration-a (gadget-value
                           (find-pane-named *application-frame* 'concentration-a-value)))
         (concentration-b (gadget-value
                           (find-pane-named *application-frame* 'concentration-b-value)))
         (drawn-width-of-cell (drawing-width-of-cell density-space-pane))
         (x (round (/ event-mouse-x drawn-width-of-cell)))
         (y (round (/ event-mouse-y drawn-width-of-cell)))
        ) ;; (space *current-space*)
    (inject-concentration (rds-space-a *current-calculation-rds*)
                          y x circle-radius concentration-a)
    (inject-concentration (rds-space-b *current-calculation-rds*)
                          y x circle-radius concentration-b)
    (draw-rds *current-calculation-rds*)))



(defmethod handle-event ((pane izumi-density-pane) (event pointer-exit-event))
  (setf (pointer-dragging pane) nil))

(defmethod handle-event ((pane izumi-density-pane) (event pointer-button-release-event))
  (setf (pointer-dragging pane) nil))

(defmethod handle-event ((pane izumi-density-pane) (event pointer-button-press-event))
  (setf (pointer-dragging pane) t))

(defmethod handle-event ((pane izumi-density-pane) (event pointer-motion-event))
  (let ((is-insert-in-circle
          (eq (gadget-id
               (gadget-value (find-pane-named *application-frame* 'insert-to-space-mode)))
              :insert-in-circle)))
    (if (and (pointer-dragging pane)
             is-insert-in-circle)
        (gui-inject-concentration pane (pointer-event-x event) (pointer-event-y event)))))

