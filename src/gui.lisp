
(in-package :izumi)




;;;; gui


(defmacro make-value-changer-gadget (access update-text 
                                     &key (min-value -1.00d0) (max-value 1.00d0))
  `(make-pane 'slider
              :min-value ,min-value :max-value ,max-value
              :value ,access
              :show-value-p nil 
              :orientation :horizontal
              :value-changed-callback
              #'(lambda (gadget value)
                  gadget value
                  (setf ,access value)
                  (format-info-area "~A ~A~%" ,update-text ,access))))


;;; mcclim                          

(define-application-frame izumi-frame ()
  ()
  (:menu-bar t)
  (:panes
   ;; main
   (info-area
    :application)
   (density-space
   (make-pane 'izumi-density-pane))
   ;; gadgets
   (space-update-mode
    (with-radio-box ()
      (radio-box-current-selection
       (make-pane 'toggle-button :label "next-button" :id :update-in-button))
      (make-pane 'toggle-button :label "auto" :id :update-in-auto)))
   (seconds-to-update
    :slider :min-value 1 :max-value 100 :value 1
    :show-value-p t :orientation :horizontal)
   (next-space-button
    :push-button
    :label "next"
    :activate-callback #'(lambda (gadget)
                           gadget
                           (perhaps-update-in-button *application-frame*)))

   ;; setup coefficient
   (coefficient-reaction-a-to-a
    (make-value-changer-gadget
     (rds-reaction-a-to-a *current-calculation-rds*)
     "coefficient reaction a-to-a: "))
   (coefficient-reaction-a-to-b
    (make-value-changer-gadget
     (rds-reaction-a-to-b *current-calculation-rds*)
     "coefficient reaction a-to-b: "))
   (coefficient-reaction-b-to-a
    (make-value-changer-gadget
     (rds-reaction-b-to-a *current-calculation-rds*)
     "coefficient reaction a-to-a: "))
   (coefficient-reaction-b-to-b
    (make-value-changer-gadget
     (rds-reaction-b-to-b *current-calculation-rds*)
     "coefficient reaction b-to-b :"))
   ;;
   (coefficient-diffusion-a
    (make-value-changer-gadget (rds-diffusion-a *current-calculation-rds*) 
                               "coefficient diffusion a :"))
   (coefficient-diffusion-b
    (make-value-changer-gadget (rds-diffusion-b *current-calculation-rds*)
                               "coefficient diffusion b :"))

   ;; reset space
   (reset-united-value-button
     :push-button
     :label "reset united-value"
     :activate-callback
     #'(lambda (gadget)
         gadget
         (let ((value-a (gadget-value
                         (find-pane-named *application-frame* 'concentration-a-value)))
               (value-b (gadget-value
                         (find-pane-named *application-frame* 'concentration-b-value))))
           (reset-to-united-value-space (rds-space-a *current-calculation-rds*) value-a)
           (reset-to-united-value-space (rds-space-b *current-calculation-rds*) value-b)
           (gui-update-of-space (rds-space-a *current-calculation-rds*) *application-frame*))))
   (reset-randomize-button
     :push-button
     :label "reset randomize"
     :activate-callback
     #'(lambda (gadget)
         gadget
         (reset-to-randomize-space (rds-space-a *current-calculation-rds*))
         (reset-to-randomize-space (rds-space-b *current-calculation-rds*))
         (gui-update-of-space (rds-space-a *current-calculation-rds*) *application-frame*)))

   ;; insert-to-space
   (insert-to-space-mode
    (with-radio-box ()
      (radio-box-current-selection
       (make-pane 'toggle-button :label "circle" :id :insert-in-circle))
      (make-pane 'toggle-button :label "none" :id :insert-in-none)))
   (radius-value
    :slider :min-value 0 :max-value 20 :value 5
    :show-value-p t :orientation :horizontal :label "aaa"
    :value-changed-callback
    #'(lambda (gadget value)
        gadget value
        (format-info-area "insert circle radius : ~A~%" value)))
   (concentration-a-value
    :slider :min-value 0.00d0 :max-value 1.00d0 :value 0.50d0
    :show-value-p nil :orientation :horizontal
    :value-changed-callback
    #'(lambda (gadget value)
        gadget value
        (format-info-area "space-a-value to reset uniform: ~A~%" value)))
   (concentration-b-value
    :slider :min-value 0.00d0 :max-value 1.00d0 :value 0.50d0
    :show-value-p nil :orientation :horizontal
    :value-changed-callback
    #'(lambda (gadget value)
        gadget value
        (format-info-area "space-b-value to reset uniform: ~A~%" value))))
  ;; layouts
  (:layouts
   (default
       (horizontally ()
         (vertically ()
           (labelling (:label "update-mode")
             (horizontally ()
               (vertically ()
                 space-update-mode
                 (labelling (:label "seconds to update") seconds-to-update))
               (labelling (:label "next") next-space-button)))
           (labelling (:label "coeffcient")
             (vertically ()
               (labelling (:label "medium reaction-rate")
                 (vertically ()
                   (horizontally ()
                     (labelling (:label "a->a") coefficient-reaction-a-to-a)
                     (labelling (:label "a->b") coefficient-reaction-a-to-b))
                   (horizontally ()
                     (labelling (:label "b->a") coefficient-reaction-b-to-a)
                     (labelling (:label "b->b") coefficient-reaction-b-to-b))))
               (labelling (:label "diffusion")
                 (horizontally ()
                   (labelling (:label "a") coefficient-diffusion-a)
                   (labelling (:label "b") coefficient-diffusion-b)))))
           (labelling (:label "reset")
             (vertically ()
               (horizontally ()
                 reset-randomize-button
                 reset-united-value-button)))
           (labelling (:label "insert")
             (vertically ()
               (horizontally ()
                 (labelling (:label "circle radius") radius-value)
                 (labelling (:label "mode") insert-to-space-mode))
               (horizontally ()
                 (labelling (:label "concentration a") concentration-a-value)
                 (labelling (:label "concentration b") concentration-b-value))
               )))
         (vertically ()
           (labelling (:label "density image") density-space)
           (labelling (:label "parameters") info-area))))))


(define-izumi-frame-command (com-quit :menu t) ()
  (frame-exit *application-frame*))


(defmethod run-frame-top-level :before ((frame izumi-frame) &key)
  (let* ((sheet (frame-top-level-sheet frame))
         (event (make-instance 'space-update-event :sheet frame)))
    (queue-event sheet event)))

;;; util

(defun format-info-area (control-string &rest format-arguments)
  (let ((info-area-stream (get-frame-pane *application-frame* 'info-area)))
    (apply #'format info-area-stream control-string format-arguments)))


;;; main loop

(defun perhaps-update-in-button (frame)
  (let ((update-mode
          (gadget-id (gadget-value (find-pane-named frame 'space-update-mode)))))
    (if (eq update-mode :update-in-button)
        (gui-update-of-space (rds-space-a *current-calculation-rds*) frame))))

(defun perhaps-update-in-auto (frame)
  (let ((update-mode
          (gadget-id (gadget-value (find-pane-named frame 'space-update-mode)))))
    (if (eq update-mode :update-in-auto)
        (gui-update-of-space (rds-space-a *current-calculation-rds*) frame))))



(defun gui-update-of-space (space frame)
  space frame
  (let ((density-space-stream (get-frame-pane frame 'density-space)))
    ;;
    density-space-stream
    (format-info-area "current-time is ~A~%" (rds-current-time *current-calculation-rds*))
    (incf (rds-current-time *current-calculation-rds*))
    ;;
    (format-info-area "updating~%")
    (multiple-value-bind (next-space-a next-space-b)
        (update-rds *current-calculation-rds*)
      (setf (rds-space-a *current-calculation-rds*) next-space-a)
      (setf (rds-space-b *current-calculation-rds*) next-space-b))
    ;;
    (format-info-area "drawing~%")
    (draw-rds *current-calculation-rds*)
    ;;
    (format-info-area "complete~%---~%")))


(defclass space-update-event (window-manager-event)
  ((val :initarg :val :accessor val)))

(defmethod handle-event ((frame izumi-frame) (event space-update-event))
  (perhaps-update-in-auto frame)
  (when (member (frame-state frame) '(:enabled :shrunk))
    (climb::schedule-event
     (frame-top-level-sheet frame)
     (make-instance 'space-update-event
                    :sheet frame
                    :val nil)
     0.5)))

;;; run

(defun run ()
  (run-frame-top-level (make-application-frame 'izumi-frame)))

